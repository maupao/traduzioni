# README - Leggimi

In questo progetto cercherò di imparare l'utilizzo di Git per la traduzione comunitaria di un articolo dal francese all'italiano.
Primi esperimenti con i comandi status, pull, add, commit, e push

Questi appunti sono assolutamente personali e probabilmente con alcuni errori o imprecisioni. Sono il frutto di appunti, esperienze ed errori. Sono ben accette osservazioni e suggerimenti volti a migliorare il flusso di lavoro. Lo scritto di seguito serve soprattutto a me come promemoria di quanto si deve fare per un utilizzo di git.

Per riassumere:

# Da fare una sola volta

Collegamento al repository on-line attraverso il comando `git clone urldelrepository.git`

tutte le volte che ci si mette al lavoro per allineare la situazione al repository
- `git status`: primo comando che fa il punto della situazione o se ci sono degli aggiornamenti da effettuare
- `git pull` per risincronizzare il tutto

Poi si può lavorare in locale su/sui file interessati. Al termine del lavoro è necessario aggiornare il db che tiene traccia dell'andamento dei lavori con questi comandi:
- `git add <file-modificato>` per aggiornare l'indice del branch in locale
- `git commit -m "motivo della modifica"`
- `git push -u origin master` oppure `git push origin master` per aggiornare il branch originale sul server collegato (in questo caso Framagit) oppure anche `git push -u origin (la situazione in locale) nome_del_branch` per infilare nel branch definito in remoto

## Creazione di una versione differente - un Branch

A partire dal materiale che si ha a disposizione si può creare una versione differente da quella originale, un branch
- `git branch nomedelbranch` per creare un nuovo ramo, branch
- `git checkout nomedelbranch` per entrare nel branch indicato e cominciare a creare la nuova versione
Consiglio: il nome del branch deve servire ad indicare per cosa serve quel determinato ramo, su che aspetto si sta lavorando

## La funzione di merge

Quando si lavora su rami differenti, nel momento in cui il lavoro nel ramo di sviluppo è maturo è possibile proporre la fusione con il ramo principale. Questa operazione si chiama merge. Prima di fondere i due rami è necessario committare il lavoro che è stato fatto. L'operazione di merge deve essere fatta dal proprietario del repository, o su un proprio ramo che si vuole inserire, oppure su una proposta di altri. (vedi il paragrafo successivo)

Poi è necessario dare questi comandi:
* `git merge upstream/master` (oppure il nome del branch in cui si vuole aggiungere le modifiche fatte)

## Per proporre la modifica ad un progetto
Per proporre la modifica ad un progetto sviluppato e mantenuto da altri bisogna prima di tutto fare un fork, poi creare un nuovo branch con il nome che fa capire dove si sta lavorando (come se fosse una cartella), e poi in remoto andare in pull request/merge e richiedere una nuova pull request. Poi si continua a lavorare nel branch creato. Al termine del lavoro si conclude la richiesta da remoto chiedendo il merge che deve essere fatto dal proprietario del progetto.

## Altri comandi
- `git remote -v` per visualizzare la situazioni dei vari repository a cui si fa riferimento
- `git remote add upstream <url del repository>` per aggiungere un repository di riferimento
- `git fetch --all` per scaricare tutti i repository, poi `git pull` per riallineare la situazione in locale se nel frattempo è cambiata tanto.

## Domande a cui non ho una risposta
* È possibile creare un nuovo repository on-line partendo dalla situazione in locale oppure bisogna agire per forza in remoto come prima battuta?
* Risposta: no, se si crea un repo in locale e vuoi portarlo on-line, devi crearne uno anche on-line e poi collegarlo con la versione in locale con il comando `git remote add upstream <url del repository>`

## Note
Un ringraziamento a @Mte90 per la revisione degli appunti. Altri eventuali errori sono da imputare a mia imperizia o non comprensione del flusso di lavoro.

## Link
Possono essere utili questi link
* [Lista giornaliera di comandi Git](https://www.giuseppemaccario.com/it/una-lista-giornaliera-di-comandi-git/) articolo di Giuseppe Maccario
* [Mini guida di sopravvivenza](https://www.evilripper.net/git-mini-guida-ai-comandi-principali/) i comandi principali
